CC = gcc
LIB_DIR = ./lib
INC_DIR = ./include
BIN_DIR = ./bin
SRC_DIR = ./src
CFLAGS = -I$(INC_DIR) -Wall

OUTPUTS += $(LIB_DIR)/libcthread.a
OUTPUTS += $(BIN_DIR)/cthread.o
OUTPUTS += $(BIN_DIR)/chelper.o

.PHONY: all
all: $(LIB_DIR)/libcthread.a

LIBCTHREAD_A_DEPS += $(BIN_DIR)/cthread.o
LIBCTHREAD_A_DEPS += $(BIN_DIR)/chelper.o
LIBCTHREAD_A_DEPS += $(BIN_DIR)/support.o
$(LIB_DIR)/libcthread.a: $(LIBCTHREAD_A_DEPS)
	ar crs $@ $^

CTHREAD_O_DEPS += $(SRC_DIR)/cthread.c
CTHREAD_O_DEPS += $(INC_DIR)/cthread.h
CTHREAD_O_DEPS += $(INC_DIR)/cdata.h
CTHREAD_O_DEPS += $(INC_DIR)/chelper.h
CTHREAD_O_DEPS += $(INC_DIR)/support.h
$(BIN_DIR)/cthread.o: $(CTHREAD_O_DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

CHELPER_O_DEPS += $(SRC_DIR)/chelper.c
CHELPER_O_DEPS += $(INC_DIR)/chelper.h
CHELPER_O_DEPS += $(INC_DIR)/cdata.h
CHELPER_O_DEPS += $(INC_DIR)/cthread.h
CHELPER_O_DEPS += $(INC_DIR)/support.h
$(BIN_DIR)/chelper.o: $(CHELPER_O_DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

.PHONY: clean
clean:
	rm -f $(OUTPUTS) && rm -rf *~
