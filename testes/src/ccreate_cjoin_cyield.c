#include <stdio.h>
#include "cthread.h"

void foo1(void *arg) {
    printf("this is foo1 before yield.\n");
    cyield();
    printf("this is foo1 after yield and ending.\n");
}

void foo2(void *arg) {
    printf("this is foo2 before yield.\n");
    cyield();
    printf("this is foo2 after yield and ending.\n");
}

int main(int argc, char *argv[]) {
    int f1 = ccreate((void * (*)(void *))&foo1, NULL, 1);
    int f2 = ccreate((void * (*)(void *))&foo2, NULL, 2);

    printf("This is main before yield.\n");

    cyield();

    printf("This is main waiting for thread %d\n", f1);

    cjoin(f1);

    printf("This is main waiting for thread %d\n", f2);

    cjoin(f2);

    printf("This is main ending.\n");

    return 0;
}

