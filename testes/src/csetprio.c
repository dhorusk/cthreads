#include <stdio.h>
#include "cthread.h"

void foo1(void *arg) {
    printf("this is foo1\n");
    cyield();
    printf("this is foo1\n");
    cyield();
    printf("this is foo1 changing thread 3 priority to superhigh and yielding\n");
    csetprio(3, 0);
    cyield();
    printf("this is foo1\n");
    cyield();
    printf("this is foo1\n");
    cyield();
    printf("this is foo1\n");
    cyield();
    printf("this is foo1\n");
    cyield();   
    printf("this is foo1 after yield and ending.\n");
}

void foo2(void *arg) {
    printf("this is foo2\n");
    cyield();
    printf("this is foo2\n");
    cyield();
    printf("this is foo2\n");
    cyield();
    printf("this is foo2\n");
    cyield();
    printf("this is foo2\n");
    cyield();
    printf("this is foo2\n");
    cyield();
    printf("this is foo2\n");
    cyield();
    printf("this is foo2 after yield and ending.\n");
}

void foo3(void *arg) {
    printf("this is foo3 and wouldn't be executed now if it wasn't for csetprio.\n");
    cyield();
    printf("this is foo3 and ending.\n");
}

int main(int argc, char *argv[]) {
    int f1 = ccreate((void * (*)(void *))&foo1, NULL, 1);
    int f2 = ccreate((void * (*)(void *))&foo2, NULL, 1);
    ccreate((void * (*)(void *))&foo3, NULL, 3);
    
    printf("This is main waiting for other threads\n");

    cjoin(f1);
    cjoin(f2);
    
    printf("This is main ending.\n");

    return 0;
}
