#include <stdio.h>
#include "cthread.h"

csem_t mutex;

void foo1() {
    printf("this is foo1 starting\n");
    printf("this is foo1 yielding\n");
    cyield();
    printf("this is foo1 trying to enter its critical region\n");
	cwait(&mutex);
    printf("this is foo1 inside its critical region\n");
    printf("this is foo1 yielding\n");
    cyield();
    printf("this is foo1 leaving its critical region\n");
    csignal(&mutex);
    printf("this is foo1 outside its critical region\n");
    printf("this is foo1 yielding\n");
    cyield();
    printf("this is foo1 yielding\n");
    cyield();
    printf("this is foo1 yielding\n");
    cyield();   
    printf("this is foo1 ending\n");
}

void foo2() {
    printf("this is foo2 starting\n");
    printf("this is foo2 yielding\n");
    cyield();
    printf("this is foo2 trying to enter its critical region\n");
	cwait(&mutex);
    printf("this is foo2 inside its critical region\n");
    printf("this is foo2 yielding\n");
    cyield();
    printf("this is foo2 leaving its critical region\n");
    csignal(&mutex);
    printf("this is foo2 outside its critical region\n");
    printf("this is foo2 yielding\n");
    cyield();
    printf("this is foo2 yielding\n");
    cyield();
    printf("this is foo2 yielding\n");
    cyield();   
    printf("this is foo2 ending\n");
}

int main(int argc, char *argv[]) {
    int f1 = ccreate((void * (*)(void *))&foo1, NULL, 0);
    int f2 = ccreate((void * (*)(void *))&foo2, NULL, 0);

	csem_init(&mutex, 1);

    printf("This is main waiting for other threads\n");

    cjoin(f1);
    cjoin(f2);

    printf("This is main ending\n");

    return 0;
}
