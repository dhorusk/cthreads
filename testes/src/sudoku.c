
/*
 * Exemplo de teste.
 * O objetivo desse teste é testar ccreate, csetprio, cyield, cjoin
 */
#include <stdio.h>
#include <stdlib.h>
#include "../include/support.h"
#include "../include/cthread.h"


void* linhas (void* teste);
void* colunas (void* teste);
void* square (void* index);

int tid[11];

			
int sudoku[9][9] = { {9,4,7,1,6,2,3,5,8},	//****************
		     {6,1,3,8,5,7,9,2,4},	//* Q1 * Q4 * Q7 *
		     {8,5,2,4,9,3,1,7,6},      	//****************
		     {1,2,9,3,8,4,5,6,7},	//* Q2 * Q5 * Q8 *
		     {5,7,8,9,2,6,4,3,1},	//****************
		     {3,6,4,7,1,5,2,8,9},	//* Q3 * Q6 * Q9 *
		     {2,9,1,6,3,8,7,4,5},	//****************
	    	     {7,8,5,2,4,1,6,9,3},	// ^Esquema de como funcionam os quadrantes
		     {4,3,6,5,7,9,8,1,2} }; 

int main()
{	
	int i;
	int teste[9] = {0,1,2,3,4,5,6,7,8};

	printf("Eu sou a main na criacao!\n");
	
	tid[0] = ccreate (linhas, NULL, 1);	
	tid[1] = ccreate (colunas, NULL, 1);

	printf("Eu sou a main trocando as prioridades !\n");

	csetprio(tid[0], 0); // Verificar linhas é prioridade (csetprio test)
	csetprio(tid[1], 0); // Verificar colunas é prioridade (csetprio test)

	printf("Prioridades trocadas !\n");

	for(i=2; i < 11; i++)
	{
		tid[i] = ccreate(square, (void*)&teste[i-2], 2);  // less priority
	}
	

	printf("Eu sou a main indo esperar pelas outras threads...\n");
	for(i=0; i<11; i++) {
		if (i==2)
			printf("Indo verificar os quadrantes...\n");
		cjoin(tid[i]);
	}

	printf("Sudoku valido!!!\n");


	return 0;

}

void* linhas (void* empty) 
{
	int i, j, k;

	for(i=0; i<9; i++) {
		printf("Thread de LINHAS na linha %d \n", i+1);
		for(j=0; j<9; j++) {
			for(k=j+1; k<9; k++) {
				if(sudoku[i][j] == sudoku[i][k])
				{
					printf("Sudoku Invalido na linha: %d!!\n", i+1);
					exit(1);
				}
			}
		}
		printf("Thread de LINHAS abrindo mao da CPU...\n");
		cyield();
	}

	printf("Valido nas linhas!!\n");
	return NULL;
}

void* colunas (void* empty) 
{
	int i, j, k;

	for(i=0; i<9; i++) {
		printf("Thread de COLUNAS na coluna %d \n", i+1);
		for(j=0; j<9; j++) {
			for(k=j+1; k<9; k++){
				if(sudoku[j][i] == sudoku[k][i])
				{
					printf("Sudoku Invalido na coluna: %d!!\n", i+1);
					exit(1);
				}
			}
		}
		printf("Thread de COLUNAS abrindo mao da CPU...\n");
		cyield();
	}

	printf("Valido nas colunas!!\n");
	return NULL;

}

void* square (void* index)
 {
	int k =  *(int*)index;
	int i,j;
	int compare[9] = {0,0,0,0,0,0,0,0,0};
	int compare_counter = 0;

	printf("Verificando quadrante %d...\n", k+1);

	for(i = (k%3)*3; i<((k%3)+1)*3;i++)
	{
		for(j = ((int)(k/3))*3; j < ((int)(k/3))*3 + 3;  j++)
		{
			compare[compare_counter] = sudoku[i][j];
			compare_counter++;
		}
	}

	for(i=0; i < 9; i++)
		for(j=i+1; j<9; j++)	
			if(compare[i] == compare[j])
			{
				printf("Sudoku Invalido no quadrante: %d!!\n", k+1);
				exit(1);
			}
	printf("Quadrante %d valido!\n", k+1);
	return NULL;
}




