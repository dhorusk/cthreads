#!/bin/bash

tests=(
	barbeiro
	ccreate_cjoin_cyield
	csetprio
	cwait_csignal
	exemplo
	filosofos
#	mandel
	prodcons
	series
	teste_vetor
	series1
	series2
	sudoku
	sudoku2
	aviao
)

make clean && make

printf "\n\n\n"
printf "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

for test in "${tests[@]}"
do
    bin/$test > tests_out/$test.txt 2>&1
    output=$(diff -qs tests_out/$test.txt tests_expected/$test.txt)
    result=$(echo $output | awk '{print $NF}')
    printf "%-30s%s\n" "Testing $test:" "$result"
done

bin/mandel -t 4 -p > tests_out/mandel.txt 2>&1
output=$(diff -qs tests_out/mandel.txt tests_expected/mandel.txt)
result=$(echo $output | awk '{print $NF}')
printf "%-30s%s\n" "Testing $test:" "$result"

printf "All tests done!\n"

printf "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
printf "\n\n\n"

rm mandel.ppm
