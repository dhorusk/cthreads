/*
 * cdata.h: arquivo de inclus�o de uso apenas na gera��o da libcthread
 *
 * Esse arquivo pode ser modificado. ENTRETANTO, deve ser utilizada a TCB fornecida
 *
 * Vers�o de 25/04/2017
 *
 */
#ifndef __CDATA_H__
#define __CDATA_H__

#include <stdbool.h>
#include <ucontext.h>
#include "support.h"

//#define DEBUG

#ifdef DEBUG
    #define LOG_C(x) (printf("CALLED: %s()\n", __FUNCTION__))
    #define LOG_D(x) (printf("DEBUG: %s\n", x))
    #define LOG_W(x) (printf("WARNING: %s\n", x))
    #define LOG_E(x) (printf("ERROR: %s\n", x))
#else
    #define LOG_C(x)
    #define LOG_D(x)
    #define LOG_W(x)
    #define LOG_E(x)
#endif // DEBUG

#define TID_MAX 1000000

#define READY_QUEUES 4

#define PRIO_SUPER_HIGH 0
#define PRIO_HIGH       1
#define PRIO_MEDIUM     2
#define PRIO_LOW        3

#define PROCST_CRIACAO  0
#define PROCST_APTO     1
#define PROCST_EXEC     2
#define PROCST_BLOQ     3
#define PROCST_TERMINO  4

#define STATE_CREATE    PROCST_CRIACAO
#define STATE_READY     PROCST_APTO
#define STATE_EXEC      PROCST_EXEC
#define STATE_BLOCK     PROCST_BLOQ
#define STATE_END       PROCST_TERMINO

/* N�O ALTERAR ESSA struct */
typedef struct s_TCB { 
    int tid;            // identificador da thread
    int state;          // estado em que a thread se encontra
                        // 0: Cria��o; 1: Apto; 2: Execu��o; 3: Bloqueado e 4: T�rmino
    int ticket;         // "bilhete" de loteria da thread, para uso do escalonador
    ucontext_t context; // contexto de execu��o da thread (SP, PC, GPRs e recursos) 
} TCB_t; 

/* scheduler */
typedef struct s_scheduler {
    bool lib_init;                      // init control variable
    int next_free_tid;                  // next available tid value
    TCB_t *exec_thread;                 // currently executing thread
    PFILA2 ready_queues[READY_QUEUES];  // ready state queues
    TCB_t **blocked_threads;            // array of pointers to threads that called cjoin()
                                        // the index represents the tid of the waited thread
                                        // reminder: blocked threads waiting for a semaphore
                                        // are stored in the semaphore
    PFILA2 semaphore_queue;             // queue of pointers to all the semaphores used
} scheduler_t;

scheduler_t g_scheduler;
ucontext_t g_destruct_context;
char *g_destruct_stack;

#endif // __CDATA_H__
