#ifndef __CHELPER_H__
#define __CHELPER_H__

#include <stdio.h>
#include <stdbool.h>
#include "cdata.h"
#include "cthread.h"

/* forward declarations of all helper functions */

void initializeLibrary(void);
bool isPrioValid(int prio);
int reserveFreeTID(void);
void addToExec(TCB_t *thread_tcb);
void addToReadyQueue(TCB_t *thread_tcb);
TCB_t *getThreadFromExec(int tid);
TCB_t *getThreadFromReady(int tid);
TCB_t *getThreadFromBlocked(int tid);
TCB_t *getThreadFromSemaphores(int tid);
TCB_t *findThreadInFila2(PFILA2 fila, int tid);
void removeThreadFromReady(int tid, int prio);
void terminateCallback(void);
TCB_t *retrieveReadyThread();
bool emptyFila2(PFILA2 queue);
bool tidExists(int tid);
void dumpReadyQueues(void);
int dispatch();

#endif // __CHELPER_H__
