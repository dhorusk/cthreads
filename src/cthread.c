#include <stdlib.h>
#include <memory.h>
#include "cthread.h"
#include "cdata.h"
#include "chelper.h"


// globals are defined in cdata.h


/*
Creation of a thread: Allocation, management and initialization of the same.

Parameters:
     Start: pointer to the function that the thread will execute.
     Arg: a parameter that can be passed to the thread at its creation.
     (Note: it is a single parameter. If it is necessary to pass more than one value a pointer to a struct must be used)
     Priority: priority with which the thread must be created.
Return:
     Correct: returns a positive value representing the tid of the created thread
     Error:
         When priority is invalid it returns -1
*/

int ccreate(void* (*start)(void*), void *arg, int prio) {
    LOG_C();

    if (g_scheduler.lib_init == false)
        initializeLibrary();

    if (isPrioValid(prio) == false)
        return -1;

    /* alloc stuff for TCB */
    TCB_t *new_thread_tcb = calloc(1, sizeof(TCB_t));
    int new_tid = reserveFreeTID();
    new_thread_tcb->tid = new_tid;
    new_thread_tcb->state = STATE_CREATE;
    new_thread_tcb->ticket = prio; // TODO: check if this should be renamed

    /* alloc stuff for context */
    getcontext(&new_thread_tcb->context);
    new_thread_tcb->context.uc_link = &g_destruct_context;
    new_thread_tcb->context.uc_stack.ss_sp = calloc(SIGSTKSZ, sizeof(char));
    new_thread_tcb->context.uc_stack.ss_size = SIGSTKSZ * sizeof(char);
    makecontext(&new_thread_tcb->context, (void (*)(void))start, 1, arg);

    addToReadyQueue(new_thread_tcb);

    return new_tid;
}


/*
Changing thread priority:
     It should be used the next time the scheduler has to choose a thread to
     win a CPU. A thread does NOT lose the processor when it makes a call to csetprio.

Parameters:
     Tid: identifier of the thread whose priority will be changed.
     Priority: new thread priority.
Return:
     Correct: Returns 0 (zero).
     Error:
         When priority is invalid it returns -1
         When tid does not exist -2 returns
*/

int csetprio(int tid, int prio) {
    LOG_C();

    if (g_scheduler.lib_init == false)
        initializeLibrary();

    if (isPrioValid(prio) == false)
        return -1;

    // look in exec slot
    TCB_t *temp_thread = getThreadFromExec(tid);
    if (temp_thread != NULL) {
        temp_thread->ticket = prio;
        return 0;
    }

    // look in ready queue
    temp_thread = getThreadFromReady(tid);
    if (temp_thread != NULL) {
        int old_prio = temp_thread->ticket;
        removeThreadFromReady(tid, old_prio); // remove old position
        temp_thread->ticket = prio;
        addToReadyQueue(temp_thread); // reposition
        return 0;
    }

    // look in blocked array
    temp_thread = getThreadFromBlocked(tid);
    if (temp_thread != NULL) {
        temp_thread->ticket = prio;
        return 0;
    }

    // look in semaphores
    temp_thread = getThreadFromSemaphores(tid);
    if (temp_thread != NULL) {
        temp_thread->ticket = prio;
        return 0;
    }

    return -2;
}


/*
Voluntarily releasing the CPU:
     The thread that executed cyield returns to the enabled state, being reinserted at the end
     of one of the ready queues, according to its priority. The scheduler will be called
     to select the thread that will receive the CPU.

Return:
     Correct: Returns 0 (zero).
     Error:
         When there are no ready threads, returns -1
 */

int cyield(void) {
    LOG_C();

    if (g_scheduler.lib_init == false)
        initializeLibrary();

    // save EXEC thread
    TCB_t *EXEC_thread = g_scheduler.exec_thread;
    addToReadyQueue(EXEC_thread);

    // find new thread for EXEC
    return dispatch();
}


/*
Term Synchronization:
     One thread can be locked until another thread terminates its execution using the cjoin function.
     A particular thread can only be expected by a single other thread.
     If two or more threads do cjoin for the same thread, only the first one that made the call will be blocked.
     The other calls will immediately return with an error code.
     If the thread does not exist it returns error code.
Parameters:
     Tid: identifier of the thread whose endpoint is being awaited.
Return:
     When executed correctly: returns 0 (zero)
     Error:
         When there are no ready threads, returns -1
         When the handle of the thread is invalid it returns -2
         When the thread with this handle does not exist it returns -3
         When there is already a thread waiting for this thread given by tid returns -4

*/

int cjoin(int tid) {
    LOG_C();

    if (g_scheduler.lib_init == false)
        initializeLibrary();

    if (tid > TID_MAX)
        return -2;

    if (!tidExists(tid))
        return -3;

    if (g_scheduler.blocked_threads[tid] != NULL)
        return -4;

    // block and put in waiting-for-join array
    TCB_t *EXEC_thread = g_scheduler.exec_thread;
    EXEC_thread->state = STATE_BLOCK;
    g_scheduler.blocked_threads[tid] = EXEC_thread;

    // find new thread for EXEC
    return dispatch();
}


/*
Semaphore initialization:
     The csem_init function initializes a variable of type csem_t.
     It consists of providing a count, positive or negative,
     Which represents the existing amount of the resource controlled by the semaphore.
     To perform mutual exclusion, this initial value of the semaphore variable must be 1.
     Each semaphore variable must have a structure that registers threads that are locked, waiting for their release.
     At startup this list should be empty.

Parameters:
     Without: pointer to a variable of type csem_t. Points to a data structure that represents the semaphore variable.
     Count: value to use when starting the semaphore. Represents the amount of resources controlled by the semaphore.
Return:
     Correct: Returns 0 (zero)
     Error:
         When the semaphore is invalid it returns -1
*/

int csem_init(csem_t *sem, int count) {
    LOG_C();

    if (g_scheduler.lib_init == false)
        initializeLibrary();

    if (sem == NULL)
        return -1;

    AppendFila2(g_scheduler.semaphore_queue, sem);

    sem->count = count;
    sem->fila = malloc(sizeof(FILA2));
    CreateFila2(sem->fila);

    return 0;
}


/*
Resource request (allocation):
     The cwait primitive will be used to request an asset.
     If the resource is free, it is assigned to the thread, which will continue its execution normally;
     Otherwise the thread will be blocked and queued for that resource in the queue.
     If in the function call the count value is less than or equal to zero, the thread must be
     Put in the locked state and placed in the queue associated with the semaphore variable.
     For each call to cwait the count variable of the semaphore structure is decremented by one unit.

Parameters:
     Without: pointer to a semaphore-type variable.
Return:
     Correct: Returns 0 (zero)
     Error:
         When the semaphore is invalid, returns -1
 */

int cwait(csem_t *sem) {
    LOG_C();

    if (g_scheduler.lib_init == false)
        initializeLibrary();

    if (sem == NULL)
        return -1;

    if (sem->count <= 0) {
        // block and put in waiting-for-semaphore array
        TCB_t *EXEC_thread = g_scheduler.exec_thread;
        EXEC_thread->state = STATE_BLOCK;
        AppendFila2(sem->fila, EXEC_thread);

        // find new thread for EXEC
        dispatch();
    }

    sem->count -= 1;

    return 0;
}


/*
Resource release:
     The csignal call is to indicate that the thread is releasing the resource.
     For each call of the csignal primitive, the count variable must be incremented by one.
     If there is more than one thread locked waiting for this resource the first of them,
     according to a FIFO policy, should move to the ready state and the remaining state should remain in the locked state.

Parameters:
     Without: pointer to a semaphore-type variable.
Return:
     Correct: Returns 0 (zero).
     Error:
         When the semaphore is invalid it returns -1
*/

int csignal(csem_t *sem) {
    LOG_C();

    if (g_scheduler.lib_init == false)
        initializeLibrary();

    if (sem == NULL)
        return -1;

    sem->count += 1;

    FirstFila2(sem->fila);
    TCB_t *block_thread = GetAtIteratorFila2(sem->fila);
    DeleteAtIteratorFila2(sem->fila);
    if (block_thread != NULL)
        addToReadyQueue(block_thread);

    return 0;
}


/*
Identification of the group:
     Implementation of a function that provides the name of the students who are part of the group that
     developed the chtread library.
Parameters:
     Name: pointer to a memory area where a string containing the names of the group components and their card numbers must be written. It should be one line per component.
     Size: The maximum number of characters that can be copied to the identification string of the group components.
Return:
     Correct: Returns 0 (zero)
     Error:
         When the size is less than 0 it returns -1
*/

int cidentify(char *name, int size) {
    LOG_C();

    if (size < 0)
        return -1;

    const char identify[] = "Daniel Kelling Brum - 233059\nJessica Salvador Rodrigues da Rocha - 242294\nMurilo Wolfart - 242226";

    if (g_scheduler.lib_init == false)
        initializeLibrary();

    memset(name, 0, size);
    memcpy(name, identify, size);

    return 0;
}
