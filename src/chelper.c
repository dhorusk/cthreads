#include <stdlib.h>
#include "chelper.h"

/* helper functions */


void initializeLibrary(void) {    
    LOG_C();

    if (g_scheduler.lib_init)
        return;

    // init ready queues
    int queue_no;
    for (queue_no = 0; queue_no < READY_QUEUES; queue_no++) {
        g_scheduler.ready_queues[queue_no] = malloc(sizeof(FILA2));
        CreateFila2(g_scheduler.ready_queues[queue_no]);
    }

    // init blocked-by-cjoin array
    g_scheduler.blocked_threads = calloc(TID_MAX, sizeof(TCB_t*));

    // init semaphore queue
    g_scheduler.semaphore_queue = malloc(sizeof(FILA2));
    CreateFila2(g_scheduler.semaphore_queue);
    
    // init main thread TCB
    TCB_t *main_thread = calloc(1, sizeof(TCB_t));
    main_thread->tid = reserveFreeTID();
    main_thread->state = STATE_EXEC;
    main_thread->ticket = PRIO_SUPER_HIGH;

    // set main to EXEC slot
    g_scheduler.exec_thread = main_thread;

    // create destructor callback
    getcontext(&g_destruct_context);
    g_destruct_context.uc_link = NULL;
    g_destruct_context.uc_stack.ss_sp = calloc(SIGSTKSZ, sizeof(char));
    g_destruct_context.uc_stack.ss_size = SIGSTKSZ * sizeof(char);
    makecontext(&g_destruct_context, (void (*)(void))terminateCallback, 0);

    // end initialization
    g_scheduler.lib_init = true;
}


inline bool isPrioValid(int prio) {
    LOG_C();

    return (prio >= 0 && prio <= 3);
}


/* return the next valid tid, incrementing the counter after that */
int reserveFreeTID() {
    LOG_C();

    if (g_scheduler.next_free_tid < TID_MAX) {
        int new_tid = g_scheduler.next_free_tid;
        g_scheduler.next_free_tid += 1;
        return new_tid;
    }

    // crash instead of overflowing
    exit(-1);
}


/* if the given TID is in the exec slot, return TCB pointer */
/* else return NULL */
TCB_t *getThreadFromExec(int tid) {
    LOG_C();

    if (g_scheduler.exec_thread->tid == tid)
        return g_scheduler.exec_thread;
    else
        return NULL;
}


/* if the given TID is in the ready queues, return TCB pointer */
/* else return NULL */
TCB_t *getThreadFromReady(int tid) {
    LOG_C();

    /* for each queue, search for TID */
    int queue_no;
    for (queue_no = 0; queue_no < READY_QUEUES; queue_no++) {
        PFILA2 queue = g_scheduler.ready_queues[queue_no];

        TCB_t *found_TCB = findThreadInFila2(queue, tid);

        if (found_TCB != NULL)
            return found_TCB;
    }

    return NULL;
}


/* if the given TID is in the blocked-by-join array, return TCB pointer */
/* else return NULL */
TCB_t *getThreadFromBlocked(int tid) {
    LOG_C();

    int cur_tid_no;
    int cur_tid_max = g_scheduler.next_free_tid;
    for (cur_tid_no = 0; cur_tid_no < cur_tid_max; cur_tid_no++) {
        TCB_t *Blocked_TCB = g_scheduler.blocked_threads[cur_tid_no];
 
        if (Blocked_TCB == NULL)
            continue;

        if (Blocked_TCB->tid == tid)
            return Blocked_TCB;
    }

    return NULL;   
}


/* if the given TID is blocked by a semaphore, return TCB pointer */
/* else return NULL */
TCB_t *getThreadFromSemaphores(int tid) {
    LOG_C();

    PFILA2 sem_queue = g_scheduler.semaphore_queue;

    if(emptyFila2(sem_queue))
        return NULL;

    FirstFila2(sem_queue);

    do {
        csem_t *current_sem = GetAtIteratorFila2(sem_queue);

        TCB_t *found_TCB = findThreadInFila2(current_sem->fila, tid);

        if(found_TCB != NULL)
            return found_TCB;
    } while (NextFila2(sem_queue) != -3);

    return NULL;
}


/* if the given TID is in the given FILA2, return TCB pointer */
/* else return NULL */
TCB_t *findThreadInFila2(PFILA2 fila, int tid) {
    LOG_C();

    if (emptyFila2(fila))
        return NULL;

    // search until queue ends
    FirstFila2(fila);

    do {
        TCB_t *current_TCB = GetAtIteratorFila2(fila);

        if (current_TCB->tid == tid)
            return current_TCB;
    } while (NextFila2(fila) != -3);

    return NULL;
}


/* remove thread from the ready queue by TID */
void removeThreadFromReady(int tid, int prio) {
    LOG_C();

    PFILA2 queue = g_scheduler.ready_queues[prio];

    if (emptyFila2(queue))
        return;

    TCB_t *current_TCB = findThreadInFila2(queue, tid);
    
    if (current_TCB != NULL)
        DeleteAtIteratorFila2(queue);
}


/* find new thread for EXEC */  
TCB_t *retrieveReadyThread() {
    LOG_C();

    int queue_no;
    for (queue_no = 0; queue_no < READY_QUEUES; queue_no++) {
        PFILA2 queue = g_scheduler.ready_queues[queue_no];

        // if the queue is empty, go to the next queue
        if (emptyFila2(queue))
            continue;

        // else, return the first TCB
        FirstFila2(queue);
        TCB_t *current_TCB = GetAtIteratorFila2(queue);
        DeleteAtIteratorFila2(queue);
        return current_TCB;
    }

    return NULL;    
}


/* receive a pointer to a TCB and put it in the exec slot */
void addToExec(TCB_t *thread_tcb) {
    LOG_C();

    if (thread_tcb == NULL)
        return;

    thread_tcb->state = STATE_EXEC;
    g_scheduler.exec_thread = thread_tcb;
}


/* receive a pointer to a TCB and append it to
 * the respective queue according to its prio. */
void addToReadyQueue(TCB_t *thread_tcb) {
    LOG_C();

    if (thread_tcb == NULL)
        return;

    thread_tcb->state = STATE_READY;
    int prio = thread_tcb->ticket;
    AppendFila2(g_scheduler.ready_queues[prio], thread_tcb);
}


/* function to be called whenever a thread terminates.
 * it should deallocate everything in the TCB (inside-out) and
 * call the dispatcher. */
void terminateCallback() {
    LOG_C();

    TCB_t *END_thread = g_scheduler.exec_thread;

    END_thread->state = STATE_END; // unnecessary...

    // restore a thread that may be waiting for this tid
    TCB_t *BLOCKED_thread = g_scheduler.blocked_threads[END_thread->tid];
    if (BLOCKED_thread != NULL) {
        g_scheduler.blocked_threads[END_thread->tid] = NULL;
        addToReadyQueue(BLOCKED_thread);
    }

    // free exec thread stack
    free(END_thread->context.uc_stack.ss_sp);   

    // find and run a new ready thread
    dispatch();
}


/* check existence of tid */
inline bool tidExists(int tid) {
    LOG_C();

    return (getThreadFromExec(tid) != NULL
            || getThreadFromReady(tid) != NULL
            || getThreadFromBlocked(tid) != NULL
            || getThreadFromSemaphores(tid) != NULL);
}


/* check if a FILA2 is empty */
bool emptyFila2(PFILA2 queue) {
    LOG_C();

    bool empty = false;

    FILA2 temp = *queue;

    FirstFila2(queue);
    if (GetAtIteratorFila2(queue) == NULL)
        empty = true;

    *queue = temp;

    return empty;
}


/* select a new ready thread and execute it
 * if there is no ready thread to run, return -1 */
int dispatch() {
    LOG_C();

    TCB_t *READY_thread = retrieveReadyThread();
    TCB_t *EXEC_thread = g_scheduler.exec_thread;

    if (READY_thread == NULL)
        return -1;

    addToExec(READY_thread);
    swapcontext(&EXEC_thread->context, &READY_thread->context); 

    return 0;
}


/* dump all the tids of each ready queue */
void dumpReadyQueues() {
    LOG_C();

    int queue_no;  
    for (queue_no = 0; queue_no < READY_QUEUES; queue_no++) {
        printf("DEBUG: QUEUE %d:", queue_no);

        PFILA2 queue = g_scheduler.ready_queues[queue_no];

        FirstFila2(queue);
        while (true) {
            TCB_t *current_TCB = GetAtIteratorFila2(queue);

            if (current_TCB == NULL)
                break;

            printf(" %d", current_TCB->tid);

            NextFila2(queue);
        }

        printf("\n");
    }
}
